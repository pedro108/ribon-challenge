# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ActiveRecord::Base.transaction do
  return if CharacterClass.any?

  CharacterClass.create!(
    animation_url: '/characters/barbaro.gif',
    attack_bonus: 4,
    damage: 12,
    description: 'Bate forte feito uma mula e possui muitos pontos de vida, porém devido ao seu estilo de luta negligente, seus ataques são pouco precisos e por vezes erram o alvo.',
    hit_points: 12,
    name: 'Bárbaro'
  )

  CharacterClass.create!(
    animation_url: '/characters/arqueiro.gif',
    attack_bonus: 6,
    damage: 6,
    description: 'Sua inteligência é mais afiada que qualquer espada e suas flechas possuem velocidade e precisão mortal. Entretanto, não possui muitos músculos, por isso cuidado na hora de lutar com os grandalhões.',
    hit_points: 6,
    name: 'Arqueiro'
  )

  CharacterClass.create!(
    animation_url: '/characters/paladino.gif',
    attack_bonus: 5,
    damage: 8,
    description: 'Campeão da justiça. Jurou usar o aço de sua espada para punir todo o mal. Suas habilidades de luta são equilibradas graças aos seus longos anos de treinamento.',
    hit_points: 10,
    name: 'Paladino'
  )

  ArmorUpgrade.create!(
    health_bonus: 2,
    level: 1,
    price: 30
  )

  ArmorUpgrade.create!(
    health_bonus: 4,
    level: 2,
    price: 200
  )

  ArmorUpgrade.create!(
    health_bonus: 6,
    level: 3,
    price: 1_200
  )

  ArmorUpgrade.create!(
    health_bonus: 8,
    level: 4,
    price: 8_000
  )

  ArmorUpgrade.create!(
    health_bonus: 10,
    level: 5,
    price: 24_000
  )

  WeaponUpgrade.create!(
    attack_bonus: 3,
    damage_bonus: 2,
    level: 1,
    price: 30
  )

  WeaponUpgrade.create!(
    attack_bonus: 7,
    damage_bonus: 3,
    level: 2,
    price: 200
  )

  WeaponUpgrade.create!(
    attack_bonus: 9,
    damage_bonus: 4,
    level: 3,
    price: 1_200
  )

  WeaponUpgrade.create!(
    attack_bonus: 12,
    damage_bonus: 5,
    level: 4,
    price: 8_000
  )

  WeaponUpgrade.create!(
    attack_bonus: 15,
    damage_bonus: 6,
    level: 5,
    price: 24_000
  )

  Monster.create!(name: 'Goblin bandoleiro')
  Monster.create!(name: 'Orc capitão')
  Monster.create!(name: 'Esqueleto guerreiro')
  Monster.create!(name: 'Lich ancião')
  Monster.create!(name: 'Dragão das profundezas')

  MonsterSheet.create!(
    animation_url: '/monsters/goblin.gif',
    attack_bonus: 4,
    damage: 4,
    hit_points: 10,
    level: 1,
    monster_id: 1,
    reward: 2
  )

  MonsterSheet.create!(
    animation_url: '/monsters/orc.gif',
    attack_bonus: 8,
    damage: 10,
    hit_points: 24,
    level: 2,
    monster_id: 2,
    reward: 10
  )

  MonsterSheet.create!(
    animation_url: '/monsters/esqueleto.gif',
    attack_bonus: 12,
    damage: 16,
    hit_points: 50,
    level: 3,
    monster_id: 3,
    reward: 100
  )

  MonsterSheet.create!(
    animation_url: '/monsters/lich.gif',
    attack_bonus: 16,
    damage: 24,
    hit_points: 75,
    level: 4,
    monster_id: 4,
    reward: 1_000
  )

  MonsterSheet.create!(
    animation_url: '/monsters/dragao.gif',
    attack_bonus: 20,
    damage: 36,
    hit_points: 100,
    level: 5,
    monster_id: 5,
    reward: 10_000
  )

  Achievement.create!(
    description: 'Perca sua primeira batalha',
    name: 'Azar de principiante',
    target_id: nil,
    target_quantity: 1,
    target_type: 'deaths'
  )

  Achievement.create!(
    description: 'Perca 10 batalhas',
    name: 'A máquina está roubando',
    target_id: nil,
    target_quantity: 10,
    target_type: 'deaths'
  )

  Achievement.create!(
    description: 'Perca 25 batalhas',
    name: 'Agora vou jogar sério',
    target_id: nil,
    target_quantity: 25,
    target_type: 'deaths'
  )

  Achievement.create!(
    description: 'Perca 50 batalhas',
    name: 'Sorte no amor',
    target_id: nil,
    target_quantity: 50,
    target_type: 'deaths'
  )

  Achievement.create!(
    description: 'Perca 100 batalhas',
    name: 'Inabalável',
    target_id: nil,
    target_quantity: 100,
    target_type: 'deaths'
  )

  Achievement.create!(
    description: 'Ganhe sua primeira moeda',
    name: 'Olhe, uma moeda!',
    target_id: nil,
    target_quantity: 1,
    target_type: 'coins'
  )

  Achievement.create!(
    description: 'Ganhe 10 moedas',
    name: 'Preciso de uma carteira maior',
    target_id: nil,
    target_quantity: 10,
    target_type: 'coins'
  )

  Achievement.create!(
    description: 'Ganhe 100 moedas',
    name: 'Você não deveria estar trabalhando?',
    target_id: nil,
    target_quantity: 100,
    target_type: 'coins'
  )

  Achievement.create!(
    description: 'Ganhe 1.000 moedas',
    name: 'Se eu não comprar nada o desconto é maior!',
    target_id: nil,
    target_quantity: 1_000,
    target_type: 'coins'
  )

  Achievement.create!(
    description: 'Ganhe 10.000 moedas',
    name: 'Rei Midas',
    target_id: nil,
    target_quantity: 10_000,
    target_type: 'coins'
  )

  Achievement.create!(
    description: 'Ganhe 100.000 moedas',
    name: 'Gênio, bilionário, playboy e filantropo',
    target_id: nil,
    target_quantity: 100_000,
    target_type: 'coins'
  )

  Achievement.create!(
    description: 'Derrote 1 goblin bandoleiro',
    name: 'Uma pequena ameaça - Nível 1',
    target_id: 1,
    target_quantity: 1,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100 goblins bandoleiros',
    name: 'Uma pequena ameaça - Nível 2',
    target_id: 1,
    target_quantity: 100,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1.000 goblins bandoleiros',
    name: 'Uma pequena ameaça - Nível 3',
    target_id: 1,
    target_quantity: 1_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 10.000 goblins bandoleiros',
    name: 'Uma pequena ameaça - Nível 4',
    target_id: 1,
    target_quantity: 10_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100.000 goblins bandoleiros',
    name: 'Uma pequena ameaça - Nível 5',
    target_id: 1,
    target_quantity: 100_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1 orc capitão',
    name: 'Óh! e agora? Quem poderá nos defender? - Nível 1',
    target_id: 2,
    target_quantity: 1,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100 orcs capitães',
    name: 'Óh! e agora? Quem poderá nos defender? - Nível 2',
    target_id: 2,
    target_quantity: 100,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1.000 orcs capitães',
    name: 'Óh! e agora? Quem poderá nos defender? - Nível 3',
    target_id: 2,
    target_quantity: 1_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 10.000 orcs capitães',
    name: 'Óh! e agora? Quem poderá nos defender? - Nível 4',
    target_id: 2,
    target_quantity: 10_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100.000 orcs capitães',
    name: 'Óh! e agora? Quem poderá nos defender? - Nível 5',
    target_id: 2,
    target_quantity: 100_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1 esqueleto guerreiro',
    name: 'Ossos do ofício - Nível 1',
    target_id: 3,
    target_quantity: 1,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100 esqueletos guerreiros',
    name: 'Ossos do ofício - Nível 2',
    target_id: 3,
    target_quantity: 100,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1.000 esqueletos guerreiros',
    name: 'Ossos do ofício - Nível 3',
    target_id: 3,
    target_quantity: 1_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 10.000 esqueletos guerreiros',
    name: 'Ossos do ofício - Nível 4',
    target_id: 3,
    target_quantity: 10_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100.000 esqueletos guerreiros',
    name: 'Ossos do ofício - Nível 5',
    target_id: 3,
    target_quantity: 100_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1 lich ancião',
    name: 'Voodoo é pra jacú - Nível 1',
    target_id: 4,
    target_quantity: 1,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100 liches anciões',
    name: 'Voodoo é pra jacú - Nível 2',
    target_id: 4,
    target_quantity: 100,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1.000 liches anciões',
    name: 'Voodoo é pra jacú - Nível 3',
    target_id: 4,
    target_quantity: 1_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 10.000 liches anciões',
    name: 'Voodoo é pra jacú - Nível 4',
    target_id: 4,
    target_quantity: 10_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100.000 liches anciões',
    name: 'Voodoo é pra jacú - Nível 5',
    target_id: 4,
    target_quantity: 100_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1 dragão das profundezas',
    name: 'De volta para as profundezas - Nível 1',
    target_id: 5,
    target_quantity: 1,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100 dragões das profundezas',
    name: 'De volta para as profundezas - Nível 2',
    target_id: 5,
    target_quantity: 100,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 1.000 dragões das profundezas',
    name: 'De volta para as profundezas - Nível 3',
    target_id: 5,
    target_quantity: 1_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 10.000 dragões das profundezas',
    name: 'De volta para as profundezas - Nível 4',
    target_id: 5,
    target_quantity: 10_000,
    target_type: 'monsters'
  )

  Achievement.create!(
    description: 'Derrote 100.000 dragões das profundezas',
    name: 'De volta para as profundezas - Nível 5',
    target_id: 5,
    target_quantity: 100_000,
    target_type: 'monsters'
  )
end
