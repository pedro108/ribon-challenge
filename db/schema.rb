# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_15_201012) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "achievements", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.string "target_type", null: false
    t.integer "target_id"
    t.integer "target_quantity", null: false
    t.index ["target_type", "target_id", "target_quantity"], name: "achievements_search_index"
  end

  create_table "armor_upgrades", force: :cascade do |t|
    t.integer "level", null: false
    t.integer "price", null: false
    t.integer "health_bonus", null: false
  end

  create_table "character_classes", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.integer "attack_bonus", null: false
    t.integer "damage", null: false
    t.integer "hit_points", null: false
    t.string "animation_url", null: false
  end

  create_table "character_sheets", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "character_class_id"
    t.integer "coins", null: false
    t.integer "defeated_monsters", null: false
    t.integer "deaths", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "armor_upgrade_id"
    t.bigint "weapon_upgrade_id"
    t.index ["armor_upgrade_id"], name: "index_character_sheets_on_armor_upgrade_id"
    t.index ["character_class_id"], name: "index_character_sheets_on_character_class_id"
    t.index ["user_id"], name: "index_character_sheets_on_user_id"
    t.index ["weapon_upgrade_id"], name: "index_character_sheets_on_weapon_upgrade_id"
  end

  create_table "collected_coins", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "value", null: false
    t.index ["user_id"], name: "index_collected_coins_on_user_id"
  end

  create_table "deaths", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "timestamp", null: false
    t.index ["user_id"], name: "index_deaths_on_user_id"
  end

  create_table "killed_monsters", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "monster_id"
    t.index ["monster_id"], name: "index_killed_monsters_on_monster_id"
    t.index ["user_id"], name: "index_killed_monsters_on_user_id"
  end

  create_table "monster_sheets", force: :cascade do |t|
    t.bigint "monster_id"
    t.integer "level", null: false
    t.integer "attack_bonus", null: false
    t.integer "damage", null: false
    t.integer "hit_points", null: false
    t.integer "reward", null: false
    t.string "animation_url", null: false
    t.index ["monster_id"], name: "index_monster_sheets_on_monster_id"
  end

  create_table "monsters", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "trophies", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "achievement_id"
    t.datetime "conquered_at", null: false
    t.datetime "notified_at"
    t.index ["achievement_id"], name: "index_trophies_on_achievement_id"
    t.index ["user_id"], name: "index_trophies_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", null: false
    t.datetime "joined_at", null: false
  end

  create_table "weapon_upgrades", force: :cascade do |t|
    t.integer "level", null: false
    t.integer "price", null: false
    t.integer "attack_bonus", null: false
    t.integer "damage_bonus", null: false
  end

  add_foreign_key "character_sheets", "armor_upgrades"
  add_foreign_key "character_sheets", "character_classes"
  add_foreign_key "character_sheets", "users"
  add_foreign_key "character_sheets", "weapon_upgrades"
  add_foreign_key "collected_coins", "users"
  add_foreign_key "deaths", "users"
  add_foreign_key "killed_monsters", "monsters"
  add_foreign_key "killed_monsters", "users"
  add_foreign_key "monster_sheets", "monsters"
  add_foreign_key "trophies", "achievements"
  add_foreign_key "trophies", "users"
end
