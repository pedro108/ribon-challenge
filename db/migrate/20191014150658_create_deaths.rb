class CreateDeaths < ActiveRecord::Migration[6.0]
  def change
    create_table :deaths do |t|
      t.belongs_to :user, foreign_key: true, index: true
      t.datetime :timestamp, null: false
    end
  end
end
