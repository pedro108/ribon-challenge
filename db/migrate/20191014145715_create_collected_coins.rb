class CreateCollectedCoins < ActiveRecord::Migration[6.0]
  def change
    create_table :collected_coins do |t|
      t.belongs_to :user, foreign_key: true, index: true
      t.integer :value, null: false
    end
  end
end
