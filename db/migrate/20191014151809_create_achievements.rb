class CreateAchievements < ActiveRecord::Migration[6.0]
  def change
    create_table :achievements do |t|
      t.string :name, null: false
      t.string :description, null: false
      t.string :target_type, null: false
      t.integer :target_id
      t.integer :target_quantity, null: false
    end

    add_index :achievements, [:target_type, :target_id, :target_quantity], name: :achievements_search_index
  end
end
