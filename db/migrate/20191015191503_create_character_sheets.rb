class CreateCharacterSheets < ActiveRecord::Migration[6.0]
  def change
    create_table :character_sheets do |t|
      t.belongs_to :user, foreign_key: true, index: true
      t.belongs_to :character_class, foreign_key: true

      t.integer :coins, null: false
      t.integer :defeated_monsters, null: false
      t.integer :deaths, null: false

      t.timestamps
    end
  end
end
