class AddWeaponUpgradeToCharacterSheet < ActiveRecord::Migration[6.0]
  def change
    add_belongs_to :character_sheets, :weapon_upgrade, foreign_key: true, null: true
  end
end
