class CreateArmorUpgrades < ActiveRecord::Migration[6.0]
  def change
    create_table :armor_upgrades do |t|
      t.integer :level, null: false
      t.integer :price, null: false
      t.integer :health_bonus, null: false
    end
  end
end
