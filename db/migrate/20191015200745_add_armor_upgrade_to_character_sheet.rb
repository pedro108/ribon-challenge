class AddArmorUpgradeToCharacterSheet < ActiveRecord::Migration[6.0]
  def change
    add_belongs_to :character_sheets, :armor_upgrade, foreign_key: true, null: true
  end
end
