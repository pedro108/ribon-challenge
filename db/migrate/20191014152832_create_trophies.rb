class CreateTrophies < ActiveRecord::Migration[6.0]
  def change
    create_table :trophies do |t|
      t.belongs_to :user, foreign_key: true, index: true
      t.belongs_to :achievement, foreign_key: true
      t.datetime :conquered_at, null: false
      t.datetime :notified_at
    end
  end
end
