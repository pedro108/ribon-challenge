class CreateKilledMonsters < ActiveRecord::Migration[6.0]
  def change
    create_table :killed_monsters do |t|
      t.belongs_to :user, foreign_key: true, index: true
      t.belongs_to :monster, foreign_key: true, index: true
    end
  end
end
