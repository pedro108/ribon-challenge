import React from "react"
import Card from "../../sharedComponents/card";
import PropTypes from "prop-types";

class MonsterSelectionMenuItem extends React.Component {
  render() {
    return (
      <Card className="monster-selection-menu-item">
        <div className="monster-image">
          <img src={this.props.monster.monster_sheet.animation_url} alt={this.props.monster.name}/>
        </div>
        <span className="monster-name">{this.props.monster.name}</span>
        <span className="description">{`Desafio de nível ${this.props.monster.monster_sheet.level}`}</span>
        <div className="attributes-list">
          <p className="attribute">
            <span className="attribute-name">Recompensa:</span>
            <span>{`${this.props.monster.monster_sheet.reward.toLocaleString()} ${this.props.monster.monster_sheet.reward > 1 ? 'moedas' : 'moeda'}`}</span>
          </p>
        </div>
        <button className="button" onClick={this.selectMonster.bind(this)} type="button">Enfrentar</button>
      </Card>
    );
  }

  selectMonster() {
    return this.props.onMonsterSelected(this.props.monster);
  }
}

MonsterSelectionMenuItem.propTypes = {
  monster: PropTypes.object.isRequired,
  onMonsterSelected: PropTypes.func.isRequired
};

export default MonsterSelectionMenuItem
