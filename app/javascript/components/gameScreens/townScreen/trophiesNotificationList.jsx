import React from "react"
import TrophiesNotificationsListItem from "./trophiesNotificationListItem";
import PropTypes from "prop-types";

class TrophiesNotificationsList extends React.Component {
  render() {
    return (
      <div className="notifications-list" id="notifications-list">
        {this.renderNotifications()}
      </div>
    );
  }

  renderNotifications() {
    const newTrophies = this.props.userTrophies.filter((trophy) => {
      return !trophy.notified_at;
    });

    return newTrophies.map(function (trophy, index) {
      return (
        <TrophiesNotificationsListItem
          key={index}
          onNotificationClosed={this.props.onNotificationClosed}
          trophy={trophy}/>
      );
    }.bind(this));
  }
}

TrophiesNotificationsList.propTypes = {
  onNotificationClosed: PropTypes.func.isRequired,
  userTrophies: PropTypes.array.isRequired
};

export default TrophiesNotificationsList
