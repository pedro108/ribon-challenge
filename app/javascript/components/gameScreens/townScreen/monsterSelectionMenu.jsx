import React from "react"
import PropTypes from "prop-types";
import MonsterSelectionMenuItem from "./monsterSelectionMenuItem";

class MonsterSelectionMenu extends React.Component {
  render() {
    return (
      <div>
        <div className="monster-selection-menu">
          {this.renderMenuItems()}
        </div>
      </div>
    );
  }

  renderMenuItems() {
    return this.props.monsters.map((monster, index) => {
      return (
        <MonsterSelectionMenuItem onMonsterSelected={this.props.onMonsterSelected} monster={monster} key={index}/>
      );
    });
  }
}

MonsterSelectionMenu.propTypes = {
  monsters: PropTypes.array.isRequired,
  onMonsterSelected: PropTypes.func.isRequired
};

export default MonsterSelectionMenu
