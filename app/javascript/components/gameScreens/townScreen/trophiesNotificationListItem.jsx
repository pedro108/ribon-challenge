import React from "react"
import PropTypes from "prop-types";

class TrophiesNotificationsListItem extends React.Component {
  render() {
    return (
      <div className="notification">
        <div className="notification-body">
          <span className="title">{this.props.trophy.achievement.name}</span>
          <span>{this.props.trophy.achievement.description}</span>
        </div>
        <div className="notification-footer">
          <a className="close-button" href="#" onClick={this.markTrophyAsNotified.bind(this)}>fechar</a>
        </div>
      </div>
    );
  }

  markTrophyAsNotified(event) {
    event.preventDefault();

    const requestUrl = `/api/trophies/${this.props.trophy.id}`;
    const requestMethod = 'PUT';
    const requestHeaders = {Accept: 'application/json', 'Content-Type': 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod};

    fetch(requestUrl, requestOptions).then(this.props.onNotificationClosed);
  }
}

TrophiesNotificationsListItem.propTypes = {
  onNotificationClosed: PropTypes.func.isRequired,
  trophy: PropTypes.object.isRequired
};

export default TrophiesNotificationsListItem
