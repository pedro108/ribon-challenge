import React from "react"
import PropTypes from "prop-types";
import Card from "../sharedComponents/card";

class TrophiesScreen extends React.Component {
  render() {
    return (
      <div className="trophies-screen">
        <Card>
          <a onClick={this.props.onWindowClose} href="#">Voltar</a>
          <div className="attributes-list">
            {this.renderAchievementsList()}
          </div>
        </Card>
      </div>
    );
  }

  renderAchievementsList() {
    return this.props.achievements.map(function (achievement, index) {
      const achievementUnlocked = this.props.trophies.find((trophy) => trophy.achievement.id === achievement.id);

      return (
        <div className={`attribute ${!!achievementUnlocked ? 'conquered' : ''}`} key={index}>
          <span className="attribute-name">{achievement.name}</span>
          <span className="attribute-value">{achievement.description}</span>
        </div>
      );
    }.bind(this));
  }
}

TrophiesScreen.propTypes = {
  achievements: PropTypes.array.isRequired,
  onWindowClose: PropTypes.func.isRequired,
  trophies: PropTypes.array.isRequired
};

export default TrophiesScreen
