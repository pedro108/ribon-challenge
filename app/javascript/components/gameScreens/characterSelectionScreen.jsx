import React from "react"
import CharacterSelectionMenu from "./characterSelectionScreen/characterSelectionMenu";
import PropTypes from "prop-types";
import SavedGameSelectionMenu from "./characterSelectionScreen/savedGameSelectionMenu";

class CharacterSelectionScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      characterClasses: []
    }
  }

  componentDidMount() {
    this.fetchAvailableCharacterClasses();
  }

  render() {
    return (
      <div className="screen character-selection-screen">
        {this.props.currentUser.character_sheets.length > 0 && this.renderSavedGameSelectionMenu()}
        {this.renderCharacterSelectionMenu()}
      </div>
    );
  }

  renderCharacterSelectionMenu() {
    return <CharacterSelectionMenu
      characterClasses={this.state.characterClasses}
      currentUser={this.props.currentUser}
      onCharacterSelected={this.props.onCharacterSelected}/>;
  }

  renderSavedGameSelectionMenu() {
    return <SavedGameSelectionMenu
      characterSheets={this.props.currentUser.character_sheets}
      onCharacterSelected={this.props.onCharacterSelected}/>;
  }

  fetchAvailableCharacterClasses() {
    const requestUrl = `/api/character_classes`;
    const requestMethod = 'GET';
    const requestHeaders = {Accept: 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then(this.setAvailableCharacterClasses.bind(this));
    });
  }

  setAvailableCharacterClasses(characterClasses) {
    this.setState({characterClasses: characterClasses})
  }
}

CharacterSelectionScreen.propTypes = {
  currentUser: PropTypes.object.isRequired,
  onCharacterSelected: PropTypes.func.isRequired,
};

export default CharacterSelectionScreen
