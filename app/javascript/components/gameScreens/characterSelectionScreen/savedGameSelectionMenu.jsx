import React from "react"
import PropTypes from "prop-types";
import SavedGameSelectionMenuItem from "./savedGameSelectionMenuItem";

class SavedGameSelectionMenu extends React.Component {
  render() {
    return (
      <div>
        <span className="title">Selecione um jogo salvo para continuar</span>
        <div className="saved-game-selection-menu">
          {this.renderMenuItems()}
        </div>
      </div>
    );
  }

  renderMenuItems() {
    return this.props.characterSheets.map((characterSheet, index) => {
      return (
        <SavedGameSelectionMenuItem onGameSelected={this.continueFromSavedGame.bind(this)} characterSheet={characterSheet} key={index}/>
      );
    });
  }

  continueFromSavedGame(selectedCharacterSheet) {
    return this.props.onCharacterSelected(selectedCharacterSheet)
  }
}

SavedGameSelectionMenu.propTypes = {
  characterSheets: PropTypes.array.isRequired,
  onCharacterSelected: PropTypes.func.isRequired
};

export default SavedGameSelectionMenu
