import React from "react"
import PropTypes from "prop-types";
import CharacterSelectionMenuItem from "./characterSelectionMenuItem";

class CharacterSelectionMenu extends React.Component {
  render() {
    return (
      <div>
        <span className="title">Selecione um personagem para começar um novo jogo</span>
        <div className="character-selection-menu">
          {this.renderMenuItems()}
        </div>
      </div>
    );
  }

  renderMenuItems() {
    return this.props.characterClasses.map((characterClass, index) => {
      return (
        <CharacterSelectionMenuItem
          onCharacterSelected={this.createCharacterSheet.bind(this)}
          characterClass={characterClass}
          key={index}/>
      );
    });
  }

  createCharacterSheet(selectedCharacterClassId) {
    const requestUrl = '/api/character_sheets';
    const requestMethod = 'POST';
    const requestBody = JSON.stringify({user_id: this.props.currentUser.id, character_class_id: selectedCharacterClassId});
    const requestHeaders = {Accept: 'application/json', 'Content-Type': 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod, body: requestBody};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then(this.props.onCharacterSelected);
    });
  }
}

CharacterSelectionMenu.propTypes = {
  currentUser: PropTypes.object.isRequired,
  characterClasses: PropTypes.array.isRequired,
  onCharacterSelected: PropTypes.func.isRequired
};

export default CharacterSelectionMenu
