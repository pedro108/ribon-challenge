import Resolver from '../helpers/resolver';
import PropTypes from "prop-types";

const BattleStatsType = {
  playerRoll: PropTypes.number.isRequired,
  monsterRoll: PropTypes.number.isRequired,
  totalPlayerAttack: PropTypes.number.isRequired,
  totalMonsterAttack: PropTypes.number.isRequired,
  totalPlayerDamage: PropTypes.number.isRequired,
  totalMonsterDamage: PropTypes.number.isRequired,
  monsterReward: PropTypes.number.isRequired,
  playerCoins: PropTypes.number.isRequired
};

const BattleResultType = {
  playerCurrentHealth: PropTypes.number.isRequired,
  monsterCurrentHealth: PropTypes.number.isRequired,
  turnResultMessage: PropTypes.string.isRequired,
  battleResultMessage: PropTypes.string.isRequired,
  playerDefeated: PropTypes.bool,
  monsterDefeated: PropTypes.bool
};

export default (function BattleResolver() {
  return Object.freeze({
    resolve(battleStats, battleResult) {
      PropTypes.checkPropTypes(BattleStatsType, battleStats);
      PropTypes.checkPropTypes(BattleResultType, battleResult);

      return Resolver(battleStats, battleResult)
        .resolve(playerCriticalMilho)
        .resolve(monsterCriticalMilho)
        .resolve(bothCriticalFail)
        .resolve(playerCriticalFail)
        .resolve(monsterCriticalFail)
        .resolve(playerCriticalHits)
        .resolve(monsterCriticalHits)
        .resolve(playerHits)
        .resolve(monsterHits)
        .resolve(draw)
        .result();
    }
  })
})();

function buildConditionResolver(condition, newResultCalculator) {
  return function(stats, result) {
    let newResult = { ...result };

    if (condition(stats)) {
      newResult = { ...newResult, ...newResultCalculator(stats, result) }

      if (newResult.playerCurrentHealth <= 0) {
        newResult = { ...newResult, ...buildDefeatResults(stats) };
      } else if(newResult.monsterCurrentHealth <= 0) {
        newResult = { ...newResult, ...buildVictoryResults(stats) };
      }

      return [true, newResult];
    }

    return [false, newResult];
  }
}

const playerCriticalMilho = buildConditionResolver(
  stats => stats.playerRoll === 1 && stats.monsterRoll === 20,
  (stats, result) => ({
    turnResultMessage: `Porra Milho que vexame! Você tropeça nas suas próprias calças, acertando a si mesmo, enquanto o monstro acerta um ponto vital de seu corpo. Você toma ${stats.totalPlayerDamage + (stats.totalMonsterDamage * 2)} de dano. Que vergonha!`,
    playerCurrentHealth: result.playerCurrentHealth - Math.min(stats.totalPlayerDamage + (stats.totalMonsterDamage * 2), result.playerCurrentHealth)
  })
);

const monsterCriticalMilho = buildConditionResolver(
  stats => stats.playerRoll === 20 && stats.monsterRoll === 1,
  (stats, result) => ({
    turnResultMessage: `Sucesso decisivo total! O monstro ${randomFumble()} e te dá a oportunidade de realizar um ataque mortal, causando ${stats.totalMonsterDamage + (stats.totalPlayerDamage * 2)} de dano!`,
    monsterCurrentHealth: result.monsterCurrentHealth - Math.min(stats.totalMonsterDamage + (stats.totalPlayerDamage * 2), result.monsterCurrentHealth)
  })
);

const bothCriticalFail = buildConditionResolver(
  stats => stats.playerRoll === 1 && stats.monsterRoll === 1,
  (stats, result) => ({
    turnResultMessage: `Galhofada em conjunto! Você e o monstro acertam a si próprios e tomam o próprio dano!`,
    playerCurrentHealth: result.playerCurrentHealth - Math.min(stats.totalPlayerDamage, result.playerCurrentHealth),
    monsterCurrentHealth: result.monsterCurrentHealth - Math.min(stats.totalMonsterDamage, result.monsterCurrentHealth)
  })
);

const playerCriticalFail = buildConditionResolver(
  stats => stats.playerRoll === 1,
  (stats, result) => ({
    turnResultMessage: `Falha crítica! Você ${randomFumble()} enquanto o monstro te acerta, tentando finalizar a batalha sem piedade. Você toma ${stats.totalPlayerDamage + stats.totalMonsterDamage} de dano!`,
    playerCurrentHealth: result.playerCurrentHealth - Math.min(stats.totalPlayerDamage + stats.totalMonsterDamage, result.playerCurrentHealth)
  })
);

const monsterCriticalFail = buildConditionResolver(
  stats => stats.monsterRoll === 1,
  (stats, result) => ({
    turnResultMessage: `Falha crítica do monstro! Ele ${randomFumble()} e você aproveita a oportunidade para golpea-lo de forma certeira, causando ${stats.totalMonsterDamage + stats.totalPlayerDamage} de dano!`,
    monsterCurrentHealth: result.monsterCurrentHealth - Math.min(stats.totalMonsterDamage + stats.totalPlayerDamage, result.monsterCurrentHealth)
  })
);

const playerCriticalHits = buildConditionResolver(
  stats => stats.playerRoll === 20,
  (stats, result) => ({
    turnResultMessage: `Sucesso decisivo! Você acertou um ponto vital do monstro e causou ${stats.totalPlayerDamage * 2} de dano!`,
    monsterCurrentHealth: result.monsterCurrentHealth - Math.min(stats.totalPlayerDamage * 2, result.monsterCurrentHealth)
  })
);

const monsterCriticalHits = buildConditionResolver(
  stats => stats.monsterRoll === 20,
  (stats, result) => ({
    turnResultMessage: `Sucesso decisivo do monstro! Ele achou uma falha crucial em suas defesas e te causou ${stats.totalMonsterDamage * 2} de dano!`,
    playerCurrentHealth: result.playerCurrentHealth - Math.min(stats.totalMonsterDamage * 2, result.playerCurrentHealth)
  })
);

const playerHits = buildConditionResolver(
  stats => stats.totalPlayerAttack > stats.totalMonsterAttack,
  (stats, result) => ({
    turnResultMessage: `Você acertou e causou ${stats.totalPlayerDamage} de dano!`,
    monsterCurrentHealth: result.monsterCurrentHealth - Math.min(stats.totalPlayerDamage, result.monsterCurrentHealth)
  })
);

const monsterHits = buildConditionResolver(
  stats => stats.totalPlayerAttack < stats.totalMonsterAttack,
  (stats, result) => ({
    turnResultMessage: `Você tomou ${stats.totalMonsterDamage} de dano!`,
    playerCurrentHealth: result.playerCurrentHealth - Math.min(stats.totalMonsterDamage, result.playerCurrentHealth)
  })
);

const draw = buildConditionResolver(
  stats => stats.totalPlayerAttack === stats.totalMonsterAttack,
  () => ({
    turnResultMessage: 'Empate! Seus golpes se cruzam e ninguém sofre dano nessa rodada.'
  })
);

function buildDefeatResults(stats) {
  return {
    battleResultMessage: `${buildDefeatMessage(calculateLostCoins(stats))}`,
    playerDefeated: true
  };
}

function buildDefeatMessage(lostCoins) {
  return lostCoins > 0
    ? `Você foi derrotado e perdeu ${buildCoinsMessage(lostCoins)}!`
    : 'Você foi derrotado porém não tinha mais moedas para perder.';
}

function calculateLostCoins({ monsterReward, playerCoins }) {
  return Math.min(
    playerCoins,
    Math.floor(monsterReward / 2)
  );
}

function buildVictoryResults(stats) {
  return {
    battleResultMessage: `Você derrotou o monstro e ganhou ${buildCoinsMessage(stats.monsterReward)}!`,
    monsterDefeated: true
  }
}

function buildCoinsMessage(coins) {
  return `${coins.toLocaleString()} ${coins === 1 ? 'moeda' : 'moedas'}`;
}

function randomFumble() {
  return (Math.floor(Math.random() * 10 ** 6) % 2) ? 'tropeça feio' : 'toma o próprio golpe';
}
