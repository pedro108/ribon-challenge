export function roll20() {
  return rollDice(20);
}

export function rollDice(diceNumber) {
  return (getRandomNumber() % diceNumber) + 1;
}

function getRandomNumber() {
  return Math.floor(Math.random() * 10 ** 6);
}
