export default function Resolver(input, result) {
  let _result = result;
  let _resolved = false;

  return Object.freeze({
    resolve(resolver) {
      if (_resolved) {
        return this;
      }

      [_resolved, _result] =  resolver(input, result);
      return this;
    },
    result() {
      const result = { ..._result };
      _result = {};
      _resolved = false;
      return result
    }
  })
}
