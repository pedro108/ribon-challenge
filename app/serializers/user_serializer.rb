class UserSerializer < ActiveModel::Serializer
  attributes :id,
             :joined_at,
             :character_sheets,
             :trophies,
             :username

  has_many :character_sheets
  has_many :trophies
end
