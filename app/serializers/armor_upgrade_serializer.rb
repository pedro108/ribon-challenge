class ArmorUpgradeSerializer < ActiveModel::Serializer
  attributes :health_bonus,
             :id,
             :level,
             :price
end
