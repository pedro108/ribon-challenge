class MonsterSheetSerializer < ActiveModel::Serializer
  attributes :animation_url,
             :attack_bonus,
             :damage,
             :hit_points,
             :level,
             :reward
end
