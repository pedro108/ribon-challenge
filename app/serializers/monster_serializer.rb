class MonsterSerializer < ActiveModel::Serializer
  attributes :name,
             :monster_sheet,
             :id

  has_one :monster_sheet
end
