class TrophySerializer < ActiveModel::Serializer
  attributes :achievement,
             :conquered_at,
             :id,
             :notified_at

  belongs_to :achievement
end
