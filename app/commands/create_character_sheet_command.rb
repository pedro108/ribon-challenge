class CreateCharacterSheetCommand
  def initialize(user_id, character_class_id)
    @character_class = CharacterClass.find(character_class_id)
    @user_id = user_id
  end

  def execute
    CharacterSheet.create!(setup_starter_character_attributes)
  end

  private

  def setup_starter_character_attributes
    {
      character_class_id: @character_class.id,
      coins: 0,
      deaths: 0,
      defeated_monsters: 0,
      user_id: @user_id
    }
  end
end