class ListWeaponUpgradesCommand
  def execute
    WeaponUpgrade.order(:level)
  end
end