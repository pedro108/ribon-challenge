class ListUsersCommand
  def execute
    User.includes([character_sheets: [:armor_upgrade, :character_class, :weapon_upgrade], trophies: [:achievement]]).all
  end
end