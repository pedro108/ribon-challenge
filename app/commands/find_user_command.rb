class FindUserCommand
  def initialize(username)
    @username = username
  end

  def execute
    User.includes([character_sheets: [:armor_upgrade, :character_class, :weapon_upgrade], trophies: [:achievement]]).find_by_username!(@username)
  end
end