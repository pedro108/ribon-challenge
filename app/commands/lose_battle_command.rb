class LoseBattleCommand
  def initialize(character_sheet_id, monster_id)
    @character_sheet = CharacterSheet.find(character_sheet_id)
    @monster = Monster.find(monster_id)
  end

  def execute
    ActiveRecord::Base.transaction do
      create_death_record
      increase_character_death_count
      decrease_character_coins
      save_changes
      unlock_trophies_for_losing_battles
      @character_sheet
    end
  end

  private

  def unlock_trophies_for_losing_battles
    total_user_deaths = Death.where(user_id: @character_sheet.user_id).count
    CollectTrophiesCommand.new(@character_sheet.user_id, 'deaths', total_user_deaths).execute
  end

  def decrease_character_coins
    @character_sheet.coins -= [@monster.monster_sheet.reward / 2, @character_sheet.coins].min
  end

  def create_death_record
    Death.create!(user_id: @character_sheet.user_id, timestamp: DateTime.now)
  end

  def increase_character_death_count
    @character_sheet.deaths += 1
  end

  def save_changes
    @character_sheet.save!
  end
end