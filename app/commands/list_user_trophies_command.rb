class ListUserTrophiesCommand
  def initialize(user_id)
    @user_id = user_id
  end

  def execute
    Trophy.where(user_id: @user_id)
  end
end