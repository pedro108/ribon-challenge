class CollectCoinsCommand
  def initialize(character_sheet_id, amount)
    @amount = amount
    @character_sheet = CharacterSheet.find(character_sheet_id)
  end

  def execute
    ActiveRecord::Base.transaction do
      create_collected_coin_record
      increase_character_coins
      save_changes
      @character_sheet
    end
  end

  private

  def create_collected_coin_record
    CollectedCoin.create!(user_id: @character_sheet.user_id, value: @amount)
  end

  def increase_character_coins
    @character_sheet.coins += @amount
  end

  def save_changes
    @character_sheet.save!
  end
end