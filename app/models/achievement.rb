class Achievement < ApplicationRecord
  has_many :trophies

  enum target_types: { coins: 'coins', deaths: 'deaths', monsters: 'monsters' }

  validates_presence_of :description, :name, :target_quantity, :target_type
end
