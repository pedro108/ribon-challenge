class User < ApplicationRecord
  has_many :character_sheets
  has_many :collected_coins
  has_many :deaths
  has_many :killed_monsters
  has_many :trophies

  validates_presence_of :joined_at, :username
end
