class ArmorUpgrade < ApplicationRecord
  has_many :character_sheets

  validates_presence_of :health_bonus, :level, :price
end
