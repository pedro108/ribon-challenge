class Death < ApplicationRecord
  belongs_to :user

  validates_presence_of :timestamp
end
