class CharacterSheet < ApplicationRecord
  belongs_to :armor_upgrade, optional: true
  belongs_to :character_class
  belongs_to :user
  belongs_to :weapon_upgrade, optional: true

  validates_presence_of :coins, :deaths, :defeated_monsters

  def total_attack_bonus
    if weapon_upgrade.present?
      character_class.attack_bonus + weapon_upgrade.attack_bonus
    else
      character_class.attack_bonus
    end
  end

  def total_damage
    if weapon_upgrade.present?
      character_class.damage * weapon_upgrade.damage_bonus
    else
      character_class.damage
    end
  end

  def total_hit_points
    if armor_upgrade.present?
      character_class.hit_points * armor_upgrade.health_bonus
    else
      character_class.hit_points
    end
  end
end
