class Api::CharacterSheets::DeathsController < ActionController::API
  def create
    record = LoseBattleCommand.new(params[:character_sheet_id], params[:monster_id]).execute
    render json: record, serializer: CharacterSheetSerializer
  end
end
