class Api::CharacterSheets::WeaponUpgradesController < ActionController::API
  def update
    record = UpgradeWeaponCommand.new(params[:character_sheet_id], params[:weapon_upgrade_id]).execute
    render json: record, serializer: CharacterSheetSerializer
  end
end
