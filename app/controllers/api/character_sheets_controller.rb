class Api::CharacterSheetsController < ActionController::API
  def create
    record = CreateCharacterSheetCommand.new(params[:user_id], params[:character_class_id]).execute
    render json: record, serializer: CharacterSheetSerializer
  end
end
