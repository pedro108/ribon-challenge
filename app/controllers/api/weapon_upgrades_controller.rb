class Api::WeaponUpgradesController < ActionController::API
  def index
    records = ListWeaponUpgradesCommand.new.execute
    render json: records, each_serializer: WeaponUpgradeSerializer
  end
end
