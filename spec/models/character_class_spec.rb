require 'rails_helper'

RSpec.describe CharacterClass, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:character_sheets) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:animation_url) }
    it { is_expected.to validate_presence_of(:attack_bonus) }
    it { is_expected.to validate_presence_of(:damage) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:hit_points) }
    it { is_expected.to validate_presence_of(:name) }
  end
end
