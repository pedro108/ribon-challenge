require 'rails_helper'

RSpec.describe ArmorUpgrade, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:character_sheets) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:health_bonus) }
    it { is_expected.to validate_presence_of(:level) }
    it { is_expected.to validate_presence_of(:price) }
  end
end
