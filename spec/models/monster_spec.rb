require 'rails_helper'

RSpec.describe Monster, type: :model do
  describe 'associations' do
    it { is_expected.to have_one(:monster_sheet) }

    it { is_expected.to have_many(:killed_monsters) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
  end
end
