require 'rails_helper'

RSpec.describe Achievement, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:trophies) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:target_quantity) }
    it { is_expected.to validate_presence_of(:target_type) }
  end
end