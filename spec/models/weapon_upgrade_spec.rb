require 'rails_helper'

RSpec.describe WeaponUpgrade, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:character_sheets) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:attack_bonus) }
    it { is_expected.to validate_presence_of(:damage_bonus) }
    it { is_expected.to validate_presence_of(:level) }
    it { is_expected.to validate_presence_of(:price) }
  end
end
